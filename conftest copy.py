import pytest
import os
from appium import webdriver

PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)


@pytest.fixture()
def driver_setup_ios(request):
    desired_caps = {}
    desired_caps["xcodeOrgId"]="WWR444332W"
    desired_caps["xcodeSigningId"]="iPhone Developer"
    desired_caps['platformName'] = 'iOS'
    desired_caps['platformVersion'] = '11.3'
    desired_caps['deviceName'] = "sandeepchalla's iPhone"
    desired_caps['udid']='74bb81a2e7979255b856fdf38437fcf070d0502a'
    desired_caps['bundleId']='com.theatro.mangerapp'
    #desired_caps['noReset']="True"
    #desired_caps['app'] = 'https://goo.gl/Xr6As3'
    desired_caps['app'] = PATH('app/Managers.ipa')
    desired_caps['automationName'] = 'XCUITest'
    #desired_caps['waitForAppScript']="$.delay(8000); $.acceptAlert();"
    desired_caps['autoAcceptAlerts']=True
    #desired_caps['autoDismissAlerts']=True
    url = 'http://localhost:4723/wd/hub'
    request.instance.driver = webdriver.Remote(url, desired_caps)

    def teardown():
        request.instance.driver.quit()
    request.addfinalizer(teardown)
