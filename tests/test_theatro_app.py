import pytest
from screens.theatro import Theatro


@pytest.mark.usefixtures('driver_setup')
class TestTheatroAppiOS:

    def test_login(self):
        theatro = Theatro(self.driver)
        theatro.login_to_MA(self.serverHostName,self.user,self.password)
        assert theatro.wait_for_visible(self.store_element)

@pytest.mark.usefixtures('driver_setup_android')
class TestTheatroAppAndroid:

    def test_login(self):
        theatro = Theatro(self.driver)
        theatro.login_to_MA("https://itg3.theatro.com","count","Theatro1*")
        assert theatro.wait_for_visible(self.store_element)