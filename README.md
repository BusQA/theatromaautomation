## File overview

`conftest.py` holds the Appium driver setup/teardown using py.test fixtures

`/app`:

* `Managers.ipa` is a test iOS app

`/screens`:

* `screen.py` contains custom helpers for Appium that can apply to any Appium project
* `base.py` extends `screen.py` and includes general locators that aren't specific to any screen in your app
* `login.py`, `oneToOne.py` represent individual screens in the app that extends `base.py`. These files include the locators and specific functions that apply to each

`/tests`:

* `test_something.py` contains the tests and is marked to use the `driver_setup` fixture defined in `conftest.py`
