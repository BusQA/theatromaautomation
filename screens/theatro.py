from screens.base import Base


class Theatro(Base):
    server_config_dots = ('name', 'initial dots')
    server_name_input = ('xpath', '//XCUIElementTypeTextField[@value=\"https://central.theatro.com/\"]')
    confirm_server_config = ('name','OK')
    username_input = ('xpath', '//XCUIElementTypeTextField[@value=\"Email address or User name\"]')
    password_input = ('xpath', '//XCUIElementTypeSecureTextField[@value=\"Password\"]')
    login_button = ('name','Log in')


    def login_to_MA(self,servername,username,password):
        self.click_on_center_screen()
        self.click(self.server_config_dots)
        self.clear_and_send_keys(self.server_name_input,str(servername))
        self.click(self.confirm_server_config)
        self.clear_and_send_keys(self.username_input,str(username))
        self.clear_and_send_keys(self.password_input,str(password))
        self.click(self.login_button)